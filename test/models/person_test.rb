require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  test "name is not empty" do
    @person = Person.new
    
    @person.name = nil
    
    assert @person.valid?
  end
end
