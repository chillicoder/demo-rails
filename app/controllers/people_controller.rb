class PeopleController < ApplicationController
  def index
    @people = Person.all
  end
  
  def new
    @person = Person.new
  end
  
  def create
    @person = Person.new(person_params)
    
    if @person.save
      redirect_to person_path(@person)
    else
      render :new
    end
  end
  
  def show
    @person = Person.where(id:params[:id]).first
  end
  
  def edit
    @person = Person.where(id:params[:id]).first
  end
  
  def update
    @person = Person.where(id:params[:id]).first
    
    if @person.update_attributes(person_params)
      redirect_to person_path(@person)
    else
      render :edit
    end
  end
  
  private
  def person_params
    params.require(:person).permit!
  end
end
